﻿using System;
using UnityEngine;

public delegate void EnemyDamageTakenHandler ( IEnemy sender, Int32 damage );

public interface IEnemy
{
    /// <summary>
    /// Enemy's position
    /// </summary>
    Vector2 Position { get; }

    /// <summary>
    /// Enemy's goal location
    /// </summary>
    Vector2 Goal { get; }

    /// <summary>
    /// Enemy's health
    /// </summary>
    Int32 Health { get; }

    /// <summary>
    /// Sets the enemy goal location
    /// </summary>
    /// <param name="location">Goal location</param>
    void SetGoal ( Vector2 location );

    /// <summary>
    /// Sets the path for the enemy to follow
    /// </summary>
    /// <param name="path"></param>
    void ProvidePath ( Vector2[] path );

    /// <summary>
    /// Called when the enemy receives damage
    /// </summary>
    event EnemyDamageTakenHandler DamageTaken;

    /// <summary>
    /// Inflicts damage
    /// </summary>
    /// <param name="damage">Amount of damage taken</param>
    void TakeDamage ( Int32 damage );
}
