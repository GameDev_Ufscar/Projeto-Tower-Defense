﻿using System;
using UnityEngine;

internal interface ITower
{
    /// <summary>
    /// Turret's damage
    /// </summary>
    Int32 Damage { get; }

    /// <summary>
    /// Turret's fire rate (in seconds)
    /// </summary>
    Double FireRate { get; }

    /// <summary>
    /// The turret's location
    /// </summary>
    Vector2 Location { get; set; }
}
