﻿using System.Collections.Generic;
using UnityEngine;

public class Enemies : MonoBehaviour, IEnemy
{

    public int Health { get; private set; }
    public int Speed { get; private set; }
    public int Damage { get; private set; }
    public Vector2 Position { get; private set; }
    public Vector2 Goal { get; private set; }

    private Stack<Vector2> Path;

    public event EnemyDamageTakenHandler DamageTaken;

    // Use this for initialization with variables HP, SPD and DMG, DMG have a default 
    // value
    void Start (int HP, int SPD, int DMG = 1)
    {
        Health = HP;
        Speed = SPD;
        Damage = DMG;
    }

    //When enemie takes damage, it is subtracted from its health
    public void TakeDamage (int T_dmg)
    {
        Health = Health - T_dmg;
        if (DamageTaken != null)
            DamageTaken (this, T_dmg);
    }

    // Update is called once per frame to check if the enemie is still alive
    void Update ()
    {
        //if the enemie doens't have health left
        if (Health <= 0)
        {
<<<<<<< HEAD
            //It is deactivated (dies)
=======
            //It is desativated (dies)
>>>>>>> cfa9e8bdbfab214c90445c51be9bd8aed5719c0a
            gameObject.SetActive (false);
        }
    }

    //Deals damage to player
    int DealDamage ()
    {
        //I don't know how to do it yet
        return Damage;
    }

    public void SetGoal ( Vector2 location )
    {
        this.Goal = location;
    }

    public void ProvidePath ( Vector2[] path )
    {
        this.Path = new Stack<Vector2> ( path );
    }
}