﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float speed = 4f;
    public float threshold;

    private Rigidbody2D rb;

    private Transform[] points;
    private Transform target;
    private int i;
    private Vector3 v;
    private float distance;

    // Use this for initialization
    void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
        target = points[0];
        i = 1;
	}

    // Update is called once per frame
    void Update()
    {
        if (i < points.Length)
        {
            distance = Vector2.Distance(new Vector2(target.position.x, target.position.y),
                                        new Vector2(transform.position.x, transform.position.y));
            if (distance < threshold)
            {
                target = points[i];
                i++;
            }
        }


        v = (target.position - transform.position).normalized * speed;
        // transform.position += new Vector3(v.x, v.y, 0);
        //rb.MovePosition(new Vector2(transform.position.x, transform.position.y) + new Vector2(v.x, v.y));

        rb.velocity = new Vector2(v.x, v.y);

    }

    public void SetPoints(Transform[] ps)
    {
        points = ps;
    }
}
