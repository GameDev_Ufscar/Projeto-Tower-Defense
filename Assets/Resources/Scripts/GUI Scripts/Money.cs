﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Money : MonoBehaviour {

    Text text;
	// Use this for initialization
	void Start () {
        PlayerState.money = 100;
        text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        text.text = "Money: " + PlayerState.money;
	}
}
