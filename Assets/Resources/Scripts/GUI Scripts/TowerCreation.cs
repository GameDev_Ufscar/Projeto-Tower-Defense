﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerCreation : MonoBehaviour
{
    public static TowerType type = TowerType.None;
    private bool gui_button_pressed = false;

    public void BuildTowerButton(string tower_type)
    {
        if (tower_type == "Weak")
        {
            if (gui_button_pressed == true && type == TowerType.Weak)
            {
                gui_button_pressed = false;
                type = TowerType.None;
            }
            else
            {
                gui_button_pressed = true;
                type = TowerType.Weak;
            }
        }
        else if (tower_type == "Medium")
        {
            if (gui_button_pressed == true && type == TowerType.Medium)
            {
                gui_button_pressed = false;
                type = TowerType.None;
            }
            else
            {
                gui_button_pressed = true;
                type = TowerType.Medium;
            }
        }
        else if (tower_type == "Strong")
        {
            if (gui_button_pressed == true && type == TowerType.Strong)
            {
                gui_button_pressed = false;
                type = TowerType.None;
            }
            else
            {
                gui_button_pressed = true;
                type = TowerType.Strong;
            }
        }
    }

}
