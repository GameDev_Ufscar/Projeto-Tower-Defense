﻿using UnityEngine;

public class LevelBehaviour : MonoBehaviour
{
    #region Enemies
    public GameObject[] Enemies;
    #endregion Enemeies

    #region Towers
    public GameObject[] Towers;
    #endregion Towers

    // Use this for initialization
    private void Start ( )
    {
        LevelState.LevelBehaviour = this;
        LevelState.Towers = Towers;
    }
}
