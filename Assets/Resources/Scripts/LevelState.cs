﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum TowerType
{
    None,
    Weak,
    Medium,
    Strong
}

static class LevelState
{
<<<<<<< HEAD

    public const int WEAK_TOWER_COST = 10;
    public const int MEDIUM_TOWER_COST = 50;
    public const int STRONG_TOWER_COST = 100;

=======
>>>>>>> cfa9e8bdbfab214c90445c51be9bd8aed5719c0a
    private static GameObject[] Enemies;
    public static GameObject[] Towers;

    /// <summary>
    /// The player's state
    /// </summary>
    public static readonly PlayerState PlayerState = new PlayerState ( );
    public static LevelBehaviour LevelBehaviour;

    /// <summary>
    /// Finds the enemy closest to a point (returns null if none is found)
    /// </summary>
    /// <param name="location">The point to compare the distance to</param>
    /// <param name="maxDistance">The maximum distance</param>
    /// <returns></returns>
    
    //public static IEnemy GetClosestEnemy ( Vector2 location, Single maxDistance = Single.MaxValue )
    //{
    //    // Single == float
    //    var minDistance = Single.MaxValue;
    //    IEnemy target = null;
    //    foreach ( GameObject enemy in Enemies )
    //    {
    //        var distance = Vector2.Distance ( location, enemy.Position );
    //        if ( distance < minDistance && distance < maxDistance )
    //        {
    //            minDistance = distance;
    //            target = enemy;
    //        }
    //    }

    //    return target;
    //}

    /// <summary>
    /// Creates a new tower at a specified location
    /// </summary>
    /// <param name="type">Type of tower to be created</param>
    /// <param name="location">Location of the created tower</param>
    

    public static void PlaceTower ( TowerType type, Vector3 location )
    {
        location.z = -1;
        UnityEngine.Object.Instantiate(Towers[(int)type - 1], location, Quaternion.identity);
        //Debug.Log("PlaceTower called! " + type.ToString());
    }
}
