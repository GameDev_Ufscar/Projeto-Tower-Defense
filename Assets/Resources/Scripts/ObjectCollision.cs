﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCollision : MonoBehaviour
{
    public string colliderTag;
    public bool active = true;

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (!active)
            return;

        Collider2D collider = collision.collider;
        if (colliderTag == "Enemy")
            Debug.Log("Enemy hit!");

        if (collider.tag == colliderTag)
        {
            PlayerState.money += 10;
            Destroy(collider.gameObject);
            Destroy(gameObject);
        }
    }
}
