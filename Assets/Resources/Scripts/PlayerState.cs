﻿using System;

public delegate void PlayerHealthChangedHandler ( PlayerState sender, Int32 previous, Int32 current );

public delegate void PlayerMoneyChangedHandler ( PlayerState sender, Int32 previous, Int32 current );

public class PlayerState
{
    #region Health Management

    public event PlayerHealthChangedHandler PlayerHealthChanged;

    private Int32 _playerHealth;

    public Int32 PlayerHealth
    {
        get
        {
            return this._playerHealth;
        }
        set
        {
            if ( value != this._playerHealth )
            {
                var prev = this._playerHealth;
                this._playerHealth = value;
                if ( this.PlayerHealthChanged != null )
                    this.PlayerHealthChanged ( this, prev, value );
            }
        }
    }

    #endregion Health Management
<<<<<<< HEAD
    public static double life;
=======
>>>>>>> cfa9e8bdbfab214c90445c51be9bd8aed5719c0a

    #region Money Management

    public event PlayerMoneyChangedHandler PlayerMoneyChanged;

    private Int32 _playerMoney;

    public Int32 PlayerMoney
    {
        get
        {
            return this._playerMoney;
        }
        set
        {
            if ( value != this._playerMoney )
            {
                var prev = this._playerMoney;
                this._playerMoney = value;
                if ( this.PlayerMoneyChanged != null )
                    this.PlayerMoneyChanged ( this, prev, value );
            }
        }
    }

    #endregion Money Management
<<<<<<< HEAD
    public static double money;
=======
>>>>>>> cfa9e8bdbfab214c90445c51be9bd8aed5719c0a
}
