﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MathFunction
{
    Floor,
    Round,
    Ceil
};

public class SpawnEnemy : MonoBehaviour
{
    [Header("Waves Info")]
    public float waitStart = 8f;
    public float waitRepeat = 1f;
    public float waitWaves = 4f;
    public int nWaves = 3;
    public int nPerWave = 4;

    [Header("Prob Info")]
    public float inc = 0.4f;
    public float init = 1f;
    public float mult = 5f;

    [Header("Mathf Info")]
    public float logCurve = 2f;
    public MathFunction mathFunction;

    [Header("Arrays")]
    public GameObject[] enemyPrefabs;
    public Transform[] points;

    private float[] probabilities;
    private float[] range;
    private float totalProbs;

    private float incDifficulty = 2f;
    private delegate int chooseFunction();
    private chooseFunction cf;

    void Start()
    {
        cf = OptNone;
        if (mathFunction == MathFunction.Ceil)  { cf = OptFloor; }
        if (mathFunction == MathFunction.Round) { cf = OptRound; }
        if (mathFunction == MathFunction.Floor) { cf = OptCeil;  }

        probabilities = new float[enemyPrefabs.Length];
        range = new float[enemyPrefabs.Length];
        probabilities.Initialize();
        SetUpRangeArray();
        probabilities[0] = init;
        totalProbs = init;

        //InvokeRepeating("SpawnEnemyPrefab", waitStart, waitRepeat);
        StartCoroutine(SpawnEnemyWave());
    }

    public IEnumerator SpawnEnemyPrefab()
    {
        //GameObject b = Instantiate(enemyPrefab, new Vector3(transform.position.x, transform.position.y, enemyPrefab.transform.position.z), Quaternion.identity);
        //b.GetComponent<EnemyController>().SetPoints(points);

        int pos = probabilities.Length - 1;
        float x = Random.Range(0, totalProbs);
        for (int i = 0; i < probabilities.Length; i++)
        {
            x -= probabilities[i];
            if (x < 0)
            {
                pos = i;
                break;
            }
        }

        GameObject b = Instantiate(enemyPrefabs[pos],
                                   new Vector3(transform.position.x, transform.position.y, enemyPrefabs[0].transform.position.z),
                                   Quaternion.identity);

        b.GetComponent<EnemyController>().SetPoints(points);

        yield return new WaitForSeconds(0f);
    }

    public IEnumerator SpawnEnemyWave()
    {
        yield return new WaitForSeconds(waitStart);

        int newN;

        for (int i = 0; i < nWaves; i++)
        {
            newN = nPerWave - 1 + cf();
            for (int j = 0; j < newN; j++)
            {
                StartCoroutine(SpawnEnemyPrefab());
                yield return new WaitForSeconds(waitRepeat);
            }

            //Debug.Log(incDifficulty + " + " + newN);
            incDifficulty += Mathf.Log(incDifficulty, 2f);
            UpdateProbabilities();

            yield return new WaitForSeconds(waitWaves);
        }

        yield return new WaitForSeconds(0f);
    }

    private void UpdateProbabilities()
    {
        for (int i = 0; i < probabilities.Length; i++)
        {
            float t = probabilities[i] + range[i];
            if (t <= Mathf.Pow(mult, i + 1))
                probabilities[i] = t;
            else
            {
                probabilities[i] = Mathf.Pow(mult, i + 1);
                IncRange();
            }
        }

        totalProbs = 0f;
        for (int i = 0; i < probabilities.Length; i++)
        {
            totalProbs += probabilities[i];
        }
    }

    void IncRange()
    {
        for (int i = 0; i < range.Length; i++)
        {
            range[i] += inc;
        }
    }

    void SetUpRangeArray()
    {
        for (int i = 0; i < range.Length; i++)
        {
            range[i] = (range.Length - i) * inc;
        }
    }

    private int OptNone()  { return (int)incDifficulty++; }
    private int OptFloor() { return (int)Mathf.Floor(Mathf.Log(incDifficulty, logCurve)); }
    private int OptRound() { return (int)Mathf.Round(Mathf.Log(incDifficulty, logCurve)); }
    private int OptCeil()  { return (int)Mathf.Ceil(Mathf.Log(incDifficulty, logCurve));  }
}
