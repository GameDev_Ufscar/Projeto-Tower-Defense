﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileType
{
    Goal,
    Path,
    Spawn,
    TowerPlace
}

public class Tile : MonoBehaviour
{
    public TileType tileType;
    public Material matHigh;
    private Material matOld;

    public void OnMouseEnter()
    {
        matOld = GetComponent<SpriteRenderer>().material;
        GetComponent<SpriteRenderer>().material = matHigh;
    }

    public void OnMouseExit()
    {
        GetComponent<SpriteRenderer>().material = matOld;
    }

    public void OnMouseDown()
    {
        //Debug.Log("Click: " + TowerCreation.type.ToString() + " - " + tileType.ToString());
        if (tileType == TileType.TowerPlace)
        {
            if (TowerCreation.type != TowerType.None)
            {
                //Debug.Log("PlaceTower called! " + TowerCreation.type.ToString());
                //Vector2 v = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y);
<<<<<<< HEAD


                if (TowerCreation.type == TowerType.Weak)
                {
                    if (PlayerState.money >= LevelState.WEAK_TOWER_COST)
                    {
                        PlayerState.money -= LevelState.WEAK_TOWER_COST;
                        LevelState.PlaceTower(TowerCreation.type, gameObject.transform.position);
                        gameObject.SetActive(false);
                    }
                }
                else if (TowerCreation.type == TowerType.Medium)
                {
                    if (PlayerState.money >= LevelState.MEDIUM_TOWER_COST)
                    {
                        PlayerState.money -= LevelState.MEDIUM_TOWER_COST;
                        LevelState.PlaceTower(TowerCreation.type, gameObject.transform.position);
                        gameObject.SetActive(false);
                    }
                }
                else if (TowerCreation.type == TowerType.Strong)
                {
                    if (PlayerState.money >= LevelState.STRONG_TOWER_COST)
                    {
                        PlayerState.money -= LevelState.STRONG_TOWER_COST;
                        LevelState.PlaceTower(TowerCreation.type, gameObject.transform.position);
                        gameObject.SetActive(false);
                    }
                }

                
=======
                LevelState.PlaceTower(TowerCreation.type, gameObject.transform.position);
                gameObject.SetActive(false);
>>>>>>> cfa9e8bdbfab214c90445c51be9bd8aed5719c0a
            }
        }
        else
            if (TowerCreation.type != TowerType.None)
                Debug.Log("You can't build a " + TowerCreation.type.ToString() + " Tower here!");
    }
}
