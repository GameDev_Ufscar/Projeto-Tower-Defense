﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public float damage = 1f;
    public float fireRate = 1f;
    public float range = 5f;
    public Transform partToRotate;
    public Transform target;
    public Transform firePoint;
    public GameObject bullet;

<<<<<<< HEAD
    private bool searchFlag = true;

    void Awake()
    {
        InvokeRepeating("SearchTarget", 0f, 0.2f);
        InvokeRepeating("Fire", 0f, 1f/fireRate);
        target = null;
=======
    void Awake()
    {
        InvokeRepeating("SearchTarget", 0f, 0.2f);
        InvokeRepeating("Fire", 0f, 1f / fireRate);
>>>>>>> cfa9e8bdbfab214c90445c51be9bd8aed5719c0a
    }

    // Use this for initialization
    void OnMouseDown ()
    {
        if (IsInvoking())
            CancelInvoke();
        else
            InvokeRepeating("Fire", 0f, 1f/fireRate);
	}

    void Fire()
    {
        if (target == null)
            return;

<<<<<<< HEAD
        if (Vector2.Distance(target.position, transform.position) > range)
        {
            target = null;
            return;
        }

        Instantiate(bullet, new Vector3(firePoint.position.x, firePoint.position.y, bullet.transform.position.z), partToRotate.rotation);
=======
        if ((target.position - transform.position).magnitude > range)
            return;

        Instantiate(bullet, new Vector3(firePoint.position.x, firePoint.position.y, -5), partToRotate.rotation);
>>>>>>> cfa9e8bdbfab214c90445c51be9bd8aed5719c0a
    }

    void SearchTarget()
    {
<<<<<<< HEAD
        if (!searchFlag)
            return;

        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        //Debug.Log("" + enemies.Length);
        if (enemies.Length == 0)
            return;

        float distance = Vector2.Distance(enemies[0].transform.position, transform.position);
        float current = 0f;

        foreach (GameObject e in enemies)
        {
            current = Vector2.Distance(e.transform.position, transform.position);
            if (current < range && current <= distance)
            {
                target = e.transform;
                distance = current;
            }
        }

        searchFlag = false;
=======
        GameObject e = GameObject.FindWithTag("Enemy");
        if (e == null)
            target = null;
        else
            target = e.transform;
>>>>>>> cfa9e8bdbfab214c90445c51be9bd8aed5719c0a
    }

    void Update()
    {
<<<<<<< HEAD
        if (target == null)
        {
            searchFlag = true;
            return;
        }
=======
        if (target == null || target.tag != "Enemy")
            return;
>>>>>>> cfa9e8bdbfab214c90445c51be9bd8aed5719c0a

        //Vector3 d = target.position - transform.position;
        //d.z = 0f;
        //Quaternion lookAt = Quaternion.LookRotation(d);
        //Vector3 r = lookAt.eulerAngles;
        ////Vector3 r = Quaternion.Lerp(partToRotate.rotation, lookAt, Time.deltaTime * 10f).eulerAngles;
        //Debug.Log(r.x + " " + r.y + " " + r.z);
        //partToRotate.rotation = Quaternion.Euler(0f, 0f, r.z);

        //Vector3 r = partToRotate.rotation.eulerAngles;
        //partToRotate.rotation = Quaternion.Euler(r.x, r.y, r.z + Time.deltaTime * 4f);

        //Vector3 direction = (target.position - partToRotate.position).normalized;
        //Quaternion rotation = Quaternion.LookRotation(direction, partToRotate.TransformDirection(Vector3.up));
        //partToRotate.rotation = new Quaternion(0, 0, rotation.z, rotation.w);

        //if ((target.position - transform.position).magnitude > range)
        //    return;

        var dir = target.position - partToRotate.position;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        partToRotate.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
